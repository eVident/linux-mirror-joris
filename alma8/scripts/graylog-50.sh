#!/bin/bash
# exit when any command fails
set -e

# Repo-id            : graylog
# Repo-name          : graylog
# Repo-revision      : 1678102007
# Repo-updated       : Mon Mar  6 11:26:59 2023
# Repo-pkgs          : 30
# Repo-available-pkgs: 30
# Repo-size          : 10 G
# Repo-baseurl       : https://packages.graylog2.org/repo/el/stable/5.0/x86_64/
# Repo-expire        : 172800 second(s) (last: Sat Mar 18 13:57:47 2023)
# Repo-filename      : /etc/yum.repos.d/graylog.repo

BASE="/repos"

echo "# GRAYLOG 5.0"
# rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-5.0-repository_latest.rpm

tee /etc/yum.repos.d/graylog50.repo > /dev/null <<EOT
[graylog-5.0]
name=graylog-5.0
baseurl=https://packages.graylog2.org/repo/el/stable/5.0/x86_64/
gpgcheck=1
repo_gpgcheck=0
gpgkey=https://packages.graylog2.org/repo/el/stable/GPG-KEY-graylog
EOT

mkdir -p ${BASE}/packages.graylog2.org/repo/packages/
mkdir -p ${BASE}/packages.graylog2.org/repo/el/stable/5.0/x86_64/

curl -s https://packages.graylog2.org/repo/el/stable/GPG-KEY-graylog -o ${BASE}/packages.graylog2.org/repo/el/stable/GPG-KEY-graylog
curl -s https://packages.graylog2.org/repo/packages/graylog-5.0-repository_latest.rpm -o ${BASE}/packages.graylog2.org/repo/packages/graylog-5.0-repository_latest.rpm

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'graylog-5.0'       -p ${BASE}/packages.graylog2.org/repo/el/stable/5.0/x86_64/
createrepo ${BASE}/packages.graylog2.org/repo/el/stable/5.0/x86_64/
