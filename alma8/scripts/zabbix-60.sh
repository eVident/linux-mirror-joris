#!/bin/bash

BASE="/repos"

echo "# ZABBIX"
rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-release-6.0-4.el8.noarch.rpm

mkdir -p ${BASE}/repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/

curl -s https://repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-release-6.0-4.el8.noarch.rpm -o ${BASE}/repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-release-6.0-4.el8.noarch.rpm

reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'zabbix'                -p ${BASE}/repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'zabbix-agent2-plugins' -p ${BASE}/repo.zabbix.com/zabbix-agent2-plugins/1/rhel/8/x86_64/
reposync --download-metadata --downloadcomps --delete --remote-time --norepopath --repo 'zabbix-non-supported'  -p ${BASE}/repo.zabbix.com/non-supported/rhel/8/x86_64/

createrepo ${BASE}/repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/
createrepo ${BASE}/repo.zabbix.com/zabbix-agent2-plugins/1/rhel/8/x86_64/
createrepo ${BASE}/repo.zabbix.com/non-supported/rhel/8/x86_64/
