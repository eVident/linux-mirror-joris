#!/bin/bash
set -e
# Repo-id            : elasticsearch-7.x
# Repo-name          : Elasticsearch repository for 7.x packages
# Repo-revision      : 1666039856
# Repo-updated       : Mon Oct 17 21:02:20 2022
# Repo-pkgs          : 1178
# Repo-available-pkgs: 1178
# Repo-size          : 57 G
# Repo-baseurl       : https://artifacts.elastic.co/packages/oss-7.x/yum
# Repo-expire        : 172800 second(s) (last: Thu Oct 27 21:32:45 2022)
# Repo-filename      : /etc/yum.repos.d/elasticsearch.repo

BASE="/repos"

echo "# ELASTIC 7"

mkdir -p ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum

curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch -o ${BASE}/artifacts.elastic.co/GPG-KEY-elasticsearch

rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

tee /etc/yum.repos.d/elasticsearch7.repo > /dev/null <<EOT
[elasticsearch-7.x]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/oss-7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOT

reposync  --download-metadata --newest-only --downloadcomps --delete --remote-time --norepopath --repo  'elasticsearch-7.x' -p ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum
createrepo ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum
 