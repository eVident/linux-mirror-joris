#!/bin/bash
set -e
# Repo-id            : opensearch

BASE="/repos"

echo "# OpenSearch"

rpm --import https://artifacts.opensearch.org/publickeys/opensearch.pgp




mkdir -p ${BASE}/artifacts.opensearch.org/releases/bundle/opensearch/2.x/yum
mkdir -p ${BASE}/artifacts.opensearch.org/publickeys/

curl -s https://artifacts.opensearch.org/publickeys/opensearch.pgp -o ${BASE}/artifacts.opensearch.org/publickeys/opensearch.pgp

tee /etc/yum.repos.d/opensearch-2.x.repo > /dev/null <<EOT
[opensearch-2.x]
name=OpenSearch 2.x
baseurl=https://artifacts.opensearch.org/releases/bundle/opensearch/2.x/yum
enabled=1
repo_gpgcheck=1
gpgcheck=1
gpgkey=https://artifacts.opensearch.org/publickeys/opensearch.pgp
autorefresh=1
type=rpm-md

EOT

# accepte l'import de la clé GPG
yum repolist -vy

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'opensearch-2.x'       -p ${BASE}/artifacts.opensearch.org/releases/bundle/opensearch/2.x/yum/

createrepo ${BASE}/artifacts.opensearch.org/releases/bundle/opensearch/2.x/yum/
