#!/bin/bash

BASE="/repos"

echo "# GITLAB"

mkdir -p ${BASE}/packages.gitlab.com/gitlab/gitlab-ce/el/8/x86_64
mkdir -p ${BASE}/packages.gitlab.com/gitlab/gitlab-ce/gpgkey

https://packages.gitlab.com/gpg.key
https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg

curl -s https://packages.gitlab.com/gpg.key -o ${BASE}/packages.gitlab.com/gpg.key
curl -s https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg -o ${BASE}/packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg

tee /etc/yum.repos.d/gitlab_gitlab-ce.repo > /dev/null <<EOT
[gitlab_gitlab-ce]
name=gitlab_gitlab-ce
baseurl=https://packages.gitlab.com/gitlab/gitlab-ce/el/8/x86_64
repo_gpgcheck=1
gpgcheck=1
enabled=1
gpgkey=https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey
       https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300

[gitlab_gitlab-ce-source]
name=gitlab_gitlab-ce-source
baseurl=https://packages.gitlab.com/gitlab/gitlab-ce/el/8/SRPMS
repo_gpgcheck=1
gpgcheck=1
enabled=1
gpgkey=https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey
       https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
EOT

# accepte l'import de la clé GPG
yum repolist -vy

reposync --download-metadata --downloadcomps --delete --newest-only --remote-time --norepopath --repo 'gitlab_gitlab-ce' -p ${BASE}/packages.gitlab.com/gitlab/gitlab-ce/el/8/x86_64

createrepo ${BASE}/packages.gitlab.com/gitlab/gitlab-ce/el/8/x86_64
