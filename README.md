# Générateur de miroirs Linux

## Description

Ce projet se base sur plusieurs conteneur pour créer une arborescence de miroir dédiés aux distributions Linux suivantes :

## Dépôts de base

- [x] Debian 9
- [x] Debian 10
- [x] Debian 11
- [x] Ubuntu 20.04
- [x] Ubuntu 22.04
- [x] CentOS 7
  - [x] base
  - [x] extras
  - [x] updates
  - [x] centosplus
- [x] AlmaLinux 8
  - [x] appstream
  - [x] baseos
  - [x] extras
  - [x] plus
  - [x] powertools  

## Dépôts tiers

- Epel
  - [x] CentOS 7
  - [x] AlmaLinux/CentOS 8

- docker
  - [x] Debian 9
  - [x] Debian 10
  - [x] Debian 11
  - [x] Ubuntu 20.04

- nvidia-docker
  - [x] Debian 9
  - [x] Debian 10
  - [x] Ubuntu 20.04

- MongoDB 4.2
  - [x] Famille RHEL 7
  - [ ] Famille RHEL 8

- GrayLog https://docs.graylog.org/v1/docs/centos

  - ```shell
    sudo rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-4.2-repository_latest.rpm
    ```

- ELASTIC 7.X

  - [x] multi-versions de la famille RHEL

- ELastic 8.X (casse GrayLog)

  - [ ] multi-versions de la famille RHEL ?

- [ ] REMI PHP

  - [x] CentOS 7
    - [x] remi-safe
    - [x] remi-modular
    - [x] remi-php56
    - [x] remi-php54
    - [x] remi-php81

  - [ ] CentOS 8

- [ ] TACPLUS  va dégager au profit de NPS sur Windaube

  - [x] CentOS 7

- [ ] Zabbix

  - [x] CentOS 7

- [ ] PostgreSQL

  - [x] CentOS 7
    - [x] pgdg-common
    - [x] pgdg14

- [ ] Microsoft VSCode




Chaque conteneur basé RHEL s'occupe de faire un mirroir de sa distribution.

Le conteneur aptbase gère toutes les distributions à base de APT (Ubuntu et Debian).

# Docker registry

Celui là est servi totalement différement, il faut utiliser l'image registry de Docker.

Détail de son utilisation dans le [docker-registry/README.md](docker-registry/README.md).

## TODO

Mettre en place :

- les cases vides
- La synchro DECOS (dsm, activeupdate, clamav)
