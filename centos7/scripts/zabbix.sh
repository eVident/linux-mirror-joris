#!/bin/bash

BASE="/repos"

echo "# ZABBIX"
rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm

mkdir -p ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/
mkdir -p ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/frontend

curl -s https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm -o ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm

reposync --download-metadata -n -l -m -d --norepopath -r 'zabbix'          -p ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/
reposync --download-metadata -n -l -m -d --norepopath -r 'zabbix-frontend' -p ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/frontend/

createrepo ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/
createrepo ${BASE}/repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/frontend/
