#!/bin/bash

BASE="/repos"

echo "# ELASTIC"
mkdir -p ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum
# mkdir -p ${BASE}/artifacts.elastic.co/packages/8.x/yum

curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch -o ${BASE}/artifacts.elastic.co/GPG-KEY-elasticsearch


rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch


tee /etc/yum.repos.d/elasticsearch.repo > /dev/null <<EOT
[elasticsearch-7.x]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/oss-7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOT

# tee /etc/yum.repos.d/elasticsearch.repo > /dev/null <<EOT
# [elasticsearch-8.x]
# name=Elasticsearch repository for 8.x packages
# baseurl=https://artifacts.elastic.co/packages/8.x/yum
# gpgcheck=1
# gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
# enabled=1
# autorefresh=1
# type=rpm-md
# EOT

reposync --download-metadata -n -l -m -d --norepopath -r 'elasticsearch-7.x' -p ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum
createrepo ${BASE}/artifacts.elastic.co/packages/oss-7.x/yum

# reposync --download-metadata -n -l -m -d --norepopath -r 'elasticsearch-8.x' -p ${BASE}/artifacts.elastic.co/packages/8.x/yum
# createrepo ${BASE}/artifacts.elastic.co/packages/8.x/yum
