#!/bin/bash

# Repo-id      : ius/x86_64
# Repo-name    : IUS for Enterprise Linux 7 - x86_64
# Repo-revision: 1683687649
# Repo-updated : Wed May 10 03:00:49 2023
# Repo-pkgs    : 212
# Repo-size    : 349 M
# Repo-baseurl : https://repo.ius.io/7/x86_64/
# Repo-expire  : 21600 second(s) (last: Mon May 22 19:44:06 2023)
#   Filter     : read-only:present
# Repo-filename: /etc/yum.repos.d/ius.repo

BASE="/repos"

echo "#  IUS (inline with upstream stable)"

yum install https://repo.ius.io/ius-release-el7.rpm -y

mkdir -p ${BASE}/repo.ius.io/7/x86_64/

curl -s https://repo.ius.io/ius-7.repo                -o ${BASE}/repo.ius.io/ius-7.repo
curl -s https://repo.ius.io/ius-archive-7.repo        -o ${BASE}/repo.ius.io/ius-archive-7.repo
curl -s https://repo.ius.io/ius-release-el7.rpm       -o ${BASE}/repo.ius.io/ius-release-el7.rpm
curl -s https://repo.ius.io/ius-testing-7.repo        -o ${BASE}/repo.ius.io/ius-testing-7.repo
curl -s https://repo.ius.io/RPM-GPG-KEY-IUS-7         -o ${BASE}/repo.ius.io/RPM-GPG-KEY-IUS-7

#reposync --download-metadata -n -l -m -d --norepopath -r 'ius' -p ${BASE}/repo.ius.io/7/x86_64/
reposync --download-metadata -l -m -d --norepopath -r 'ius' -p ${BASE}/repo.ius.io/7/x86_64/

createrepo ${BASE}/repo.ius.io/7/x86_64/
