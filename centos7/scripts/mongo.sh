#!/bin/bash

BASE="/repos"

echo "# MONGO"

mkdir -p ${BASE}/repo.mongodb.org/yum/redhat/7/mongodb-org/4.2/x86_64/
mkdir -p ${BASE}/www.mongodb.org/static/pgp/
curl -s https://www.mongodb.org/static/pgp/server-4.2.asc -o ${BASE}/www.mongodb.org/static/pgp/server-4.2.asc

tee /etc/yum.repos.d/mongodb-org.repo > /dev/null <<EOT
[mongodb-org-4.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOT
reposync --download-metadata -n -l -m -d --norepopath -r 'mongodb-org-4.2' -p ${BASE}/repo.mongodb.org/yum/redhat/7/mongodb-org/4.2/x86_64/
createrepo ${BASE}/repo.mongodb.org/yum/redhat/7/mongodb-org/4.2/x86_64/
