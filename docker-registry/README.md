# How to

## Méthode 1

- configurer le docker de la machine hote pour utiliser le mirroir local
  en créant le fichier `/etc/docker/daemon.json`
  ```json
  {
        "registry-mirrors": ["http://127.0.0.1:5000"]
  }
  ```
- lancer un registry avec `docker compose up`
```yml
version: '3.4'

services:
  monregistry:
    image: registry:2
    ports:
      - 5000:5000
    environment:
      REGISTRY_PROXY_REMOTEURL: https://registry-1.docker.io
    volumes:
      - ./data:/var/lib/registry
```
- lancer le téléchargement de toutes les images avec `poule.sh`
 
## Méthode 2

```bash
# démarre le mirroir (avec son point de montage à remplir avec les layers)
docker compose up proxy-registry
# démarre le démon dind
docker compose up docker
# démarre un client docker (pas moyen de lancer un script dans le conteneur dind, cf. la documentation)
docker compose up 
```

## Ref de documentation

- <https://www.caktusgroup.com/blog/2020/02/25/docker-image/>

## Ref des images téléchargées

- <https://hub.docker.com/_/>
- <https://hub.docker.com/_/alpine>
- <https://hub.docker.com/_/busybox>
- <https://hub.docker.com/_/centos>
- <https://hub.docker.com/_/debian>
- <https://hub.docker.com/_/docker>
- <https://hub.docker.com/_/elasticsearch>
- <https://hub.docker.com/_/gitlab/gitlab-ce>
- <https://hub.docker.com/_/golang>
- <https://hub.docker.com/_/haproxy>
- <https://hub.docker.com/_/hello-world>
- <https://hub.docker.com/_/java>
- <https://hub.docker.com/_/jenkins>
- <https://hub.docker.com/_/kibana>
- <https://hub.docker.com/_/logstash>
- <https://hub.docker.com/_/mariadb>
- <https://hub.docker.com/_/maven>
- <https://hub.docker.com/_/memcached>
- <https://hub.docker.com/_/mongo>
- <https://hub.docker.com/_/mysql>
- <https://hub.docker.com/_/nginx>
- <https://hub.docker.com/_/nicolargo/glances>
- <https://hub.docker.com/_/node>
- <https://hub.docker.com/_/openjdk>
- <https://hub.docker.com/_/php>
- <https://hub.docker.com/_/postgres>
- <https://hub.docker.com/_/python>
- <https://hub.docker.com/_/rabbitmq>
- <https://hub.docker.com/_/redis>
- <https://hub.docker.com/_/registry>
- <https://hub.docker.com/_/ruby>
- <https://hub.docker.com/_/sameersbn/squid>
- <https://hub.docker.com/_/solr>
- <https://hub.docker.com/_/sonarqube>
- <https://hub.docker.com/_/sonatype/nexus>
- <https://hub.docker.com/_/sonatype/nexus3>
- <https://hub.docker.com/_/swarm>
- <https://hub.docker.com/_/ubuntu>
